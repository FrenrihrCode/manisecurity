import React from 'react';
import {
    Text,
    View,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Dimensions
} from 'react-native';
import { Button, Image } from 'react-native-elements';
import Swiper from 'react-native-swiper';
const { width, height } = Dimensions.get('window');

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            activeIndex: 0
        }
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: 'black' }}>
                <SafeAreaView style={{ flex: 1, }}>
                    <View style={{marginTop: 10, marginHorizontal: 20, alignItems: 'center' }}>
                        <Text style={{ color: '#efb810', fontWeight: 'bold', fontSize: 32 }}>
                            Bienvenido a
                        </Text>
                        <Image style={{height: 60, width: 150}} source={require('../../assets/logoextend.png')} />
                    </View>

                    <View style={styles.container}>

                            <Swiper
                                style={styles.wrapper}
                                height={280}
                                width={250}
                                
                                paginationStyle={{
                                    bottom: -20,
                                }}
                                loop={true}

                                dot={
                                    <View
                                        style={{
                                            backgroundColor: '#FFF176',
                                            width: 13,
                                            height: 13,
                                            borderRadius: 7,
                                            marginLeft: 7,
                                            marginRight: 7
                                        }}
                                    />
                                }
                                activeDot={
                                    <View
                                        style={{
                                            backgroundColor: '#efb810',
                                            width: 13,
                                            height: 13,
                                            borderRadius: 7,
                                            marginLeft: 7,
                                            marginRight: 7
                                        }}
                                    />
                                }
                            >
                                <View style={styles.container1}>
                                    <Image style={styles.image}
                                        source={require('../../assets/1.png')} />
                                    <Text style={styles.txtslider}>Busca el departamento de tus sueños.</Text>
                                </View>
                                <View style={styles.container1}>
                                    <Image style={styles.image}
                                        source={require('../../assets/solicita1.png')} />
                                    <Text style={styles.txtslider}>Solicita el alquiler de manera sencilla.</Text>
                                </View>
                                <View style={styles.container1}>
                                    <Image style={styles.image}
                                        source={require('../../assets/contacta.png')} />
                                    <Text style={styles.txtslider}>Contacta sin problemas.</Text>
                                </View>
                            </Swiper>
                            <Text></Text>
                    </View>
                    <View style={styles.container}>
                        <Button
                            containerStyle={{width: '60%'}}
                            buttonStyle={{ backgroundColor: 'black' }}
                            titleStyle={{color: '#efb810'}}
                            raised
                            onPress={() => { this.props.navigation.navigate('Login') }}
                            title="Iniciar sesión"
                        />
                        <Button
                            containerStyle={{width: '60%', marginTop: 10}}
                            buttonStyle={{ backgroundColor: '#efb810' }}
                            titleStyle={{color: 'black'}}
                            raised
                            onPress={() => { this.props.navigation.navigate('Register') }}
                            title="Registrarse"
                            type="solid"
                        />
                    </View>
                </SafeAreaView>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container1: {
        paddingTop: 10,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    txtslider: {
        fontSize: 16,
        textAlign: 'center',
        fontWeight: 'bold',
        color: 'black'
    },
    wrapper: {
        alignItems: 'center'
    },
    image: {
        width: 230,
        height: 230
    },
    container: {
        backgroundColor: '#bfbfbf',
        marginHorizontal: 20,
        borderRadius: 20,
        paddingHorizontal: 10,
        paddingVertical: 10,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
        alignItems: 'center'
    },
});