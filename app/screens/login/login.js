import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';
import Loader from '../components/loader';
import firebase from '../../config/firebase';

function Login(props){
    const { navigation } = props;
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [error, setError] = React.useState('');
    const [loading, setLoading] = React.useState(false);
    const [isPasswordShow, setIsPasswordShow] = React.useState(true);

    const showPassword1 = () =>{
        setIsPasswordShow(!isPasswordShow)
    }

    async function onLogin(){
        setLoading(true);
        await firebase.auth().signInWithEmailAndPassword(email, password).then( async() => {
          let userId = firebase.auth().currentUser.uid;
          console.log("Usuario: "+userId+" se ha logeado");
          setError('');
          setLoading(false);
        })
        .catch(error => {
            console.log(error.toString());
            setLoading(false);
            setError('Usuario o contraseña incorrectos.');
        });
    }

    return(
        <ScrollView style={styles.scrollContainer}>
            <View style={{marginTop: 30, alignItems: 'center'}}>
                <Text style={styles.title}>Hola!</Text>
                <Text style={{color: 'white', marginBottom: 10, fontSize: 24}}>Ingrese para continuar</Text>
            </View>
            <View style={styles.container}>
                <Loader loading={loading} />
                <Input
                    value={email}
                    placeholderTextColor='#AAAAAA'
                    onChangeText={setEmail}
                    errorMessage={error}
                    keyboardType='email-address'
                    placeholder='Su email'
                    inputStyle={styles.inputStyle}
                    leftIcon={{ type: 'material', name: 'email', color: '#506982' }}
                />
                <Input
                    placeholder='Su contraseña'
                    placeholderTextColor='#AAAAAA'
                    value={password}
                    secureTextEntry={isPasswordShow}
                    onChangeText={setPassword}
                    errorMessage={error}
                    inputStyle={styles.inputStyle}
                    leftIcon={{ type: 'material', name: 'lock', color: '#506982' }}
                    rightIcon={
                    <Icon
                        type='material'
                        name={isPasswordShow ? 'visibility':'visibility-off'}
                        color='#506982'
                        onPress={showPassword1}
                    />}
                />
                <Text style={{color: '#7093B5', alignSelf: 'flex-end', marginHorizontal: 20, marginBottom: 15, marginTop: -3}} >
                    ¿Olvido su contraseña?
                </Text>
                <Button buttonStyle={styles.btnLogin} title='INGRESAR' onPress={onLogin} titleStyle={{color: '#efb810'}} />
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: '#7093B5'}} >
                        ¿No posee una cuenta?
                    </Text>
                    <Button titleStyle={{color: '#efb810'}} type='clear' title='REGISTRESE' onPress={()=>navigation.navigate('Register')} />
                </View>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322'
    },
    container:{
        flex: 1,
        backgroundColor: '#EFF0F0',
        marginHorizontal: 20,
        marginTop: 10,
        borderRadius: 20,
        padding: 15,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
    },
    title: {
        color: '#efb810',
        fontSize: 42,
        fontWeight: 'bold'
    },
    inputStyle: {
        color: '#262322'
    },
    btnLogin: {
        marginHorizontal: 30,
        marginBottom: 10,
        backgroundColor: 'black'
    }
});

export default Login