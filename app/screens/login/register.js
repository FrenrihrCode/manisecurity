import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';
import Loader from '../components/loader';
import Firebase from '../../config/firebase';

function Register(props){
    const {navigation} = props;
    const [email, setEmail] = React.useState('');
    const [emailErr, setEmailErr] = React.useState('');
    const [nombre, setNombre] = React.useState('');
    const [apellido, setApellido] = React.useState('');
    const [contraseña, setContraseña] = React.useState('');
    const [contraseñaErr, setContraseñaErr] = React.useState('');
    const [loading, setLoading] = React.useState(false);
    const [secureText, setSecureText] = React.useState(true);

    const mostrarTexto = () =>{
        setSecureText(!secureText)
    }

    async function onRegister(){
        setLoading(true);
        setEmailErr('');
        setContraseñaErr('');
        await Firebase.auth().createUserWithEmailAndPassword(email, contraseña).then(() => {
            console.log("Usuario: "+email+" se ha registrado");
            //Añadir usuario
            let user = Firebase.auth().currentUser;
            console.log("Usuario: "+user.uid+" se ha logeado");
            setLoading(false);
            user.updateProfile({
                displayName: `${nombre} ${apellido}`,
                photoURL: "https://firebasestorage.googleapis.com/v0/b/manisecurity-2639a.appspot.com/o/avatar%2Fdefault.png?alt=media&token=adef2795-797d-40cf-8f1d-af14d4a6e749"
            });
        })
        .catch(error => {
            setLoading(false);
            if (error.code === 'auth/email-already-in-use') {
                console.log('El email ya está en uso');
                setEmailErr('El email ya está en uso.');
            }
            if (error.code === 'auth/invalid-email') {
                console.log('La dirección de correo no es válida');
                setEmailErr('La dirección de correo no es válida.');
            }
            if (error.code === 'auth/weak-password') {
                console.log('La contraseña debería tener al menos 6 caracteres');
                setContraseñaErr('La contraseña debería tener al menos 6 caracteres.');
            } else {
                console.log(error);
                setEmailErr('Ocurrió un error inesperado intentelo más tarde.');
            }
        });
      } 

    return(
        <ScrollView style={styles.scrollContainer}>
            <View style={{marginTop: 25, alignItems: 'center'}}>
                <Text style={styles.title}>Cuentame,</Text>
                <Text style={{color: 'white', fontSize: 18}}>¿Cuáles son tus datos personales?</Text>
            </View>
            <View style={styles.container}>
                <Loader loading={loading} />
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1}}>
                    <Input
                        value={nombre}
                        placeholderTextColor='#A7AEB5'
                        onChangeText={setNombre}
                        placeholder='Su nombre'
                        style={{flex: 1}}
                        inputStyle={styles.inputStyle}
                        leftIcon={{ type: 'material', name: 'person', color: '#506982' }}
                    />
                    </View>
                    <View style={{flex: 1}}>
                    <Input
                        value={apellido}
                        placeholderTextColor='#A7AEB5'
                        onChangeText={setApellido}
                        placeholder='Su apellido'
                        style={{flex: 1}}
                        inputStyle={styles.inputStyle}
                        leftIcon={{ type: 'material', name: 'person', color: '#506982' }}
                    />
                    </View>
                </View>
                <Input
                    value={email}
                    placeholderTextColor='#A7AEB5'
                    onChangeText={setEmail}
                    placeholder='Su email'
                    keyboardType='email-address'
                    errorMessage={emailErr}
                    inputStyle={styles.inputStyle}
                    leftIcon={{ type: 'material', name: 'email', color: '#506982' }}
                />
                <Input
                    placeholder='Su contraseña'
                    placeholderTextColor='#A7AEB5'
                    value={contraseña}
                    secureTextEntry={secureText}
                    onChangeText={setContraseña}
                    errorMessage={contraseñaErr}
                    inputStyle={styles.inputStyle}
                    leftIcon={{ type: 'material', name: 'lock', color: '#506982' }}
                    rightIcon={
                    <Icon
                        type='material'
                        name={secureText ? 'visibility':'visibility-off'}
                        color='#506982'
                        onPress={mostrarTexto}
                    />}
                />
                <Button onPress={onRegister} buttonStyle={styles.btnLogin} titleStyle={{color: '#efb810'}} title='REGISTRARSE' />
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: '#7093B5'}} >
                        ¿Ya es miembro?
                    </Text>
                    <Button titleStyle={{color: '#efb810'}} type='clear' title='INGRESE' onPress={()=>navigation.navigate('Login')} />
                </View>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322'
    },
    container:{
        flex: 1,
        backgroundColor: '#EFF0F0',
        marginHorizontal: 16,
        marginTop: 20,
        borderRadius: 20,
        paddingHorizontal: 10,
        paddingVertical: 15,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
    },
    title: {
        color: '#efb810',
        fontSize: 28,
        fontWeight: 'bold'
    },
    inputStyle: {
        color: '#262322',
        fontSize: 16
    },
    btnLogin: {
        marginHorizontal: 30,
        marginTop: 10,
        backgroundColor: '#262322'
    }
});

export default Register