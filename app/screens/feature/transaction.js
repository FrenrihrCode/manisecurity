import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Loader from '../components/loader'
import Firebase from '../../config/firebase'
import 'firebase/firestore'

export default function transaction(props) {
    const { route, navigation } = props
    const { id } = route.params
    const [transactiondata, setTransactiondata] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    
    useEffect(() => {
        setLoading(true)
        async function getData() {
            try {
                const db = Firebase.firestore()
                db.settings({ experimentalForceLongPolling: true })
                await db.collection('transactions').doc(id).get().then( (res) =>{
                    let data = res.data();
                    setTransactiondata(data)
                    setLoading(false)
                    setError(false)
                })
            } catch (error) {
                console.log("salio un error:", error)
                setError(true)
                setLoading(false)
            }
        }
        getData()
    }, [])

    return (
        <View style={styles.container}>
            <Loader loading={loading} />
            {
                error ? 
                    <View style={styles.datacontent}>
                        <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>
                            Ocurrió un error, intentelo más tarde.
                        </Text>
                    </View>
                :   
                    <View>
                        <Text style={{ fontSize: 20, marginHorizontal: 20, fontWeight: 'bold', marginTop: 10 }}>Datos del departamento:</Text>
                        <View style={styles.datacontent}>
                            <Text style={{ color: '#efb810', fontSize: 16, fontWeight: 'bold' }}>Nombre: {transactiondata.apartmentName}</Text>
                            <Text style={{ color: '#efb810', fontSize: 16, fontWeight: 'bold' }}>Dirección: {transactiondata.apartmentLocation}</Text>
                        </View>
                        <Text style={{ fontSize: 20, marginHorizontal: 20, fontWeight: 'bold', marginTop: 10 }}>Datos del recibidor:</Text>
                        <View style={styles.datacontent}>
                            <Text style={{ color: '#efb810', fontSize: 16, fontWeight: 'bold' }}>Nombre: {transactiondata.receiverName}</Text>
                            <Text style={{ color: '#efb810', fontSize: 16, fontWeight: 'bold' }}>Email: {transactiondata.receiverEmail}</Text>
                            <Text style={{ color: '#efb810', fontSize: 16, fontWeight: 'bold' }}>Celular: {transactiondata.receiverCellphone}</Text>
                        </View>
                        <Text style={{ fontSize: 20, marginHorizontal: 20, fontWeight: 'bold', marginTop: 10 }}>Datos de la compra:</Text>
                        <View style={styles.datacontent}>
                            <Text style={{ color: '#efb810', fontSize: 16, fontWeight: 'bold' }}>Comprador: {transactiondata.generatedByName}</Text>
                            <Text style={{ color: '#efb810', fontSize: 16, fontWeight: 'bold' }}>Tajeta tipo:  {transactiondata.transactionType}</Text>
                            <Text style={{ color: '#efb810', fontSize: 16, fontWeight: 'bold' }}>Costo: S/. {transactiondata.precio}</Text>
                        </View>
                    </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    datacontent: {
        marginHorizontal: 20,
        marginTop: 10,
        backgroundColor: 'black',
        padding: 10,
        borderRadius: 15,
        elevation: 5
    }
})
