/*This is an example of Image Picker in React Native*/
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import {Button} from 'react-native-elements';
import Loader from '../components/loader';
import ImagePicker from 'react-native-image-picker';
import Firebase from '../../config/firebase';

export default class Updatephoto extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filePath: {},
      escogido: false,
      isloading: false
    };
  }
  chooseFile = () => {
    var options = {
      title: 'Seleccionar imagen',
      cancelButtonTitle: 'Cancelar',
      chooseFromLibraryButtonTitle: 'Escoger de mi galería',
      takePhotoButtonTitle: 'Tomar un foto',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        console.log(response.uri)
        console.log(response.fileName)
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          filePath: source,
          escogido: true
        });
      }
    });
  };

  updateAvatar = () => {
    /*
    const uuid = Firebase.auth().currentUser.uid;
    const fileExtension = this.state.filePath.fileName.split('.').pop();
    const fileName = `${uuid}.${fileExtension}`;
    console.log(fileName);
    let storageRef = Firebase.storage().ref();
    console.log(storageRef);
    let uri= 'data:image/jpeg;base64,' + this.state.filePath.data;
    let mime = 'image/jpeg'
    ref.put(file).then(function(snapshot) {
        console.log('Uploaded a blob or file!');
    });*/
    this.setState({isloading: true});
    this.uriToBlob(this.state.filePath.uri)
    .then((blob)=>{
      return this.uploadToFirebase(blob);
    }).then((snapshot)=>{
      snapshot.ref.getDownloadURL().then( (downloadURL) => {
        let user = Firebase.auth().currentUser;
        user.updateProfile({
            photoURL: downloadURL
        });
        this.setState({isloading: false});
        this.props.navigation.goBack();
      });
    }).catch((error)=>{
      console.log(error)
      this.setState({isloading: false});
    }); 
  }

  uriToBlob = (uri) => {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function() {
        // return the blob
        resolve(xhr.response);
      };
      xhr.onerror = function() {
        // something went wrong
        reject(new Error('uriToBlob failed'));
      };
      // this helps us get a blob
      xhr.responseType = 'blob';
      xhr.open('GET', uri, true);
      xhr.send(null);
    });
  }

  uploadToFirebase = (blob) => {
    return new Promise((resolve, reject)=>{
      const storageRef = Firebase.storage().ref();
      let uuid = Firebase.auth().currentUser.uid;
      storageRef.child(`avatar/${uuid}.jpg`).put(blob, {
        contentType: 'image/jpeg'
      }).then((snapshot)=>{
        blob.close();
        resolve(snapshot);
      }).catch((error)=>{
        reject(error);
      });
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Loader loading={this.state.isloading} />
        <View style={styles.container}>
          <Image
            source={{
              uri: 'data:image/jpeg;base64,' + this.state.filePath.data,
            }}
            style={{ width: 100, height: 100 }}
          />
          <Image
            source={{ uri: this.state.filePath.uri }}
            style={{ width: 250, height: 250 }}
          />
          <Button title="Escoger" titleStyle={{color: '#efb810'}} buttonStyle={{backgroundColor: 'black'}} onPress={this.chooseFile.bind(this)} />
          { this.state.escogido ? <Button titleStyle={{color: '#efb810'}} buttonStyle={{backgroundColor: 'black'}} title="Actualizar mi avatar" onPress={this.updateAvatar.bind(this)} /> : null}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});