import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, Image, ScrollView, ActivityIndicator } from "react-native";
import { ListItem, Avatar } from 'react-native-elements';
import Firebase from '../../config/firebase';
import 'firebase/firestore';

function Feature(props) {
    const { navigation } = props;
    const user = Firebase.auth().currentUser;
    const [isLoaded, setLoaded] = useState(false);
    const [transactions, setTransactions] = useState([]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getResponse()
        });
        return () => { unsubscribe() }
    }, [])

    const getResponse = async () => {
        setLoaded(false)
        const db = Firebase.firestore();
        db.settings({ experimentalForceLongPolling: true });
        await db.collection('users')
            .doc(user.uid).get().then((res) => {
                if (res.exists) {
                    setLoaded(true)
                    let data = res.data();
                    setTransactions(data.transactionID)
                } else {
                    setLoaded(true)
                    console.log('Document does not exist!');
                }
            });
    }

    return (
            <View style={styles.container}>
                <Text style={{ fontSize: 20, marginHorizontal: 20, fontWeight: 'bold', marginTop: 10 }}>Datos de mi cuenta</Text>
                <View style={styles.datacontent}>
                    <Avatar
                        containerStyle={{ backgroundColor: 'white' }}
                        showAccessory
                        size='large'
                        rounded
                        onPress={()=>navigation.navigate('Updatephoto')}
                        source={{
                            uri: user.photoURL
                        }}
                    />
                    <View style={{ flexDirection: 'column', marginLeft: 15, justifyContent: 'center' }}>
                        <Text style={{ color: '#efb810', fontSize: 20, fontWeight: 'bold' }}>{user.displayName}</Text>
                        <Text style={{ color: '#efb810', fontSize: 16 }}>{user.email}</Text>
                    </View>
                </View>
                <Text style={{ fontSize: 20, marginHorizontal: 20, fontWeight: 'bold', marginTop: 10 }}>Mis transacciones</Text>
                <View style={styles.datacontent}>
                    {isLoaded ? 
                        <FlatList
                            data={transactions}
                            showsVerticalScrollIndicator={false}
                            legacyImplementation={false}
                            numColumns={1}
                            keyExtractor={(item, index) => index}
                            renderItem={({ item, index }) => {
                                return (
                                    <ListItem
                                        containerStyle={{backgroundColor: 'transparent'}}
                                        titleStyle={{color: '#efb810'}}
                                        title={`Transacción N° ${item.slice(0, 4)}******`}
                                        leftIcon={{ name: 'flight-takeoff', color: 'white' }}
                                        bottomDivider
                                        onPress={()=>navigation.navigate('Transaction', {id: item})}
                                        chevron
                                    />
                                )
                            }}
                            ListEmptyComponent={() => (
                                <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>
                                    Aún no has realizado una transacción, ¿qué esperas?
                                </Text>
                            )}
                        />
                    :
                        <View style={{alignContent: 'center', flex: 1}}>
                            <ActivityIndicator color='#efb810' size='large' />
                            <Text style={{ color: 'white', fontSize: 16, fontWeight: 'bold', textAlign: 'center' }}>
                                Cargando transacciones...
                            </Text>
                        </View>
                    }
                </View>
            </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    datacontent: {
        flexDirection: 'row',
        marginHorizontal: 20,
        marginTop: 10,
        backgroundColor: 'black',
        padding: 10,
        borderRadius: 15,
        elevation: 5
    }
});

export default Feature;