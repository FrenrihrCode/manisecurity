import React, {useState} from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Input, Button, Icon, Divider,Text } from 'react-native-elements';
import { CreditCardInput } from 'react-native-input-credit-card';

function RentProcess2(props){
    const { route, navigation } = props;
    const [creditCard, setCreditCard] = useState('');
    const [type, setType] = useState('');
    //Values from previous Navigator
    const {name, email, cellphone, initialDate, finalDate, apartmentLocation, apartmentName, precio} = route.params;
    
    var dataCreditCard = ''
    var typeCreditCard = ''
    const _onFocus = () => field => {
        console.log('focusing', field);
    };
    const _onChange = () => formData => {
        console.log(JSON.stringify(formData, null, ' '));
        dataCreditCard=formData.values.number;
        typeCreditCard=formData.values.type;
        setCreditCard(dataCreditCard);
        setType(typeCreditCard);
    };

    return(
        
        <ScrollView style={styles.scrollContainer}>
            <View style={{marginTop: 10, paddingBottom:15, paddingTop:15,backgroundColor:'white'}}  
                borderRadius={15}
                width= '100%'>
                <CreditCardInput 
                    autoFocus
                    requireName
                    requireCVC
                    requirePoscalCode
                    validColor="black"
                    invalidColor="red"
                    placeholderColor="darkgray"
                    labelStyle={{color: 'black',fontSize: 12}}
                    inputStyle={{color: 'black', fontSize: 16}}
                    onFocus={_onFocus()}
                    onChange={_onChange()}
                />
            </View>
            <View style={{marginTop: 10, paddingBottom:15, paddingTop:15,marginBottom:10,backgroundColor:'white', alignItems: 'center'}}  
                borderRadius={15}
                width= '100%'>
                <Text h4 style={{paddingBottom:10}} >Informacion de Usuario</Text>
                
                <View style={{flex: 1, flexDirection: 'row' }}>
                    <View style={{width: '50%', alignItems: 'flex-end'}}>
                        <Text>A Nombre de:  </Text>
                    </View>
                    <View style={{width: '50%',  alignItems: 'flex-start'}}>
                        <Text style={{color:'grey'}}>{name}</Text>
                    </View>
                </View>
                <View style={{flex: 1, flexDirection: 'row', }}>
                    <View style={{width: '50%',  alignItems: 'flex-end'}}>
                        <Text>Correo electrónico:  </Text>
                    </View>
                    <View style={{width: '50%',  alignItems: 'flex-start'}}>
                        <Text style={{color:'grey'}}>{email}</Text>
                    </View>
                </View>
                <View style={{flex: 1, flexDirection: 'row', }}>
                    <View style={{width: '50%',  alignItems: 'flex-end'}}>
                        <Text>Celular:  </Text>
                    </View>
                    <View style={{width: '50%',   alignItems: 'flex-start'}}>
                        <Text style={{color:'grey'}}>{cellphone}</Text>
                    </View>
                </View>
                <View style={{flex: 1, flexDirection: 'row', }}>
                    <View style={{width: '50%',  alignItems: 'flex-end'}}>
                        <Text>Fecha de ocupación:  </Text>
                    </View>
                    <View style={{width: '50%',   alignItems: 'flex-start'}}>
                        <Text style={{color:'grey'}}>{initialDate}</Text>
                    </View>
                </View>
                <View style={{flex: 1, flexDirection: 'row', }}>
                    <View style={{width: '50%',  alignItems: 'flex-end'}}>
                        <Text>Fecha final:  </Text>
                    </View>
                    <View style={{width: '50%',   alignItems: 'flex-start'}}>
                        <Text style={{color:'grey'}}>{finalDate}</Text>
                    </View>
                </View>
                <View style={{flex: 1, flexDirection: 'row', }}>
                    <View style={{width: '50%',  alignItems: 'flex-end'}}>
                        <Text>Departamento a rentar:  </Text>
                    </View>
                    <View style={{width: '50%',   alignItems: 'flex-start'}}>
                        <Text style={{color:'grey'}}>{apartmentName}</Text>
                    </View>
                </View>
                <View style={{flex: 1, flexDirection: 'row', }}>
                    <View style={{width: '50%',  alignItems: 'flex-end'}}>
                        <Text>Ubicacion:  </Text>
                    </View>
                    <View style={{width: '50%',   alignItems: 'flex-start'}}>
                        <Text style={{color:'grey'}}>{apartmentLocation}</Text>
                    </View>
                </View>
                <View style={{flex: 1, flexDirection: 'row', }}>
                    <View style={{width: '50%',  alignItems: 'flex-end'}}>
                        <Text>Costo:  </Text>
                    </View>
                    <View style={{width: '50%',   alignItems: 'flex-start'}}>
                        <Text style={{color:'grey'}}>S/. {precio}</Text>
                    </View>
                </View>

                <Button
                    icon={<Icon name='credit-card' color='#efb810' size={15} />}
                    buttonStyle={{backgroundColor: 'black', marginTop:20, marginHorizontal: 0, marginBottom: 0, borderRadius: 5}}
                    //onPress={showvalue}
                    onPress={()=> navigation.navigate('RentProcess3',{
                        creditCard : creditCard,
                        type: type,
                        name: name,
                        email:email,
                        cellphone:cellphone,
                        initialDate:initialDate,
                        finalDate:finalDate,
                        apartmentName: apartmentName, 
                        apartmentLocation: apartmentLocation,
                        precio: precio
                    })
                    }
                    title=' Pagar' 
                    type='solid'
                    titleStyle={{color: '#efb810'}}
                />
            </View>


        </ScrollView>
    );
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322'
    },
    container:{
        flex: 1,
        backgroundColor: '#EFF0F0',
        marginHorizontal: 20,
        marginTop: 10,
        borderRadius: 20,
        padding: 15,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
    },
    title: {
        color: '#efb810',
        fontSize: 42,
        fontWeight: 'bold'
    },
    inputStyle: {
        color: '#262322'
    },
    btnLogin: {
        marginHorizontal: 30,
        marginBottom: 10,
        backgroundColor: 'black'
    }
});

export default RentProcess2