import React, { useEffect,useState } from 'react';
import { StyleSheet, View, ScrollView, Keyboard } from 'react-native';
import { Input, Button, Card, Divider } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import { TouchableOpacity } from 'react-native-gesture-handler';

function RentProcess1(props){
    const { route, navigation } = props;
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [cellphone, setCellphone] = useState('');
    const [initialDate, setInitialDate] = useState('Fecha Inicio');
    const [finalDate, setFinalDate] = useState('Fecha Final');
    const [errorName, setErrorName] = useState('  ');
    const [errorEmail, setErrorEmail] = useState(' ');
    const [errorCellphone, setErrorCellphone] = useState(' ');
    const [errorInitialDate, setErrorInitialDate] = useState('');
    const [errorFinalDate, setErrorFinalDate] = useState('');
    //Values from previous Navigator
    const {apartmentName, apartmentLocation, precio} = route.params;
    //Setter para el DatePicker
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };
    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };
    const handleConfirm = (date) => {
        setInitialDate(moment(date).format("YYYY-MM-DD"));
        hideDatePicker();
    };

    //CONTROLS FOR SECOND DATEPICKER
    const [isDatePickerVisible2, setDatePickerVisibility2] = useState(false);
    const showDatePicker2 = () => {
        setDatePickerVisibility2(true);
    };
    const hideDatePicker2 = () => {
        setDatePickerVisibility2(false);
    };
    const handleConfirm2 = (date) => {
        setFinalDate(moment(date).format("YYYY-MM-DD"));
        hideDatePicker2();
    };

    //Validation helper functions
    const changeName = name =>{
        var re = /^[a-zA-Z\s]*$/;
        if(re.test(name)) {
            setName(name);
            setErrorName('');
        }
        else{
           setName(name);
           setErrorName('Ingrese un nombre valido'); 
        }
    };

    const changeEmail = email =>{
        var re = /\S+@\S+\.\S+/;
        if(re.test(email)) {
            setEmail(email);
            setErrorEmail('');
        }
        else{
            setEmail(email);
            setErrorEmail('Ingrese un email valido');
        }
    };

    const changeCellphone = cellphone =>{
        var re = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
        if(re.test(cellphone)) {
            setCellphone(cellphone);
            setErrorCellphone('');
        }
        else{
            setCellphone(cellphone);
            setErrorCellphone('Campo obligatorio');
        }
    };
    
    //Navigation screen and error handling
    const validacion = () =>{
        if(errorName=="" && errorCellphone==""){ //&& errorEmail==""
            console.log('VALIDACION OK');
            navigation.navigate('RentProcess2',{
                name : name,
                email : email,
                cellphone: cellphone,
                initialDate: initialDate,
                finalDate: finalDate,
                apartmentName: apartmentName, 
                apartmentLocation: apartmentLocation,
                precio: precio
            });
        }else{
            console.log("else")
            if(name === ""){
                setErrorName('Campo obligatorio.');
            } 
            if(email===""){
                setErrorEmail('Ingrese un email valido.')
            }
            if(cellphone===""){
                setErrorCellphone('Campo obligatorio.')
            }
        }
    }
    /*TESTING BUTTON
    const showvalue=()=>{
        console.log(initialDate);
        console.log(finalDate);
        console.log(name);
        console.log(email);
        console.log(cellphone);
        console.log(errorCellphone);
        console.log(errorEmail);
        console.log(errorName);
        console.log(errorInitialDate);
        console.log(errorFinalDate);
    }*/
    //var duration = moment.duration(end.diff(initialDate));
    //var months = duration.asMonths;
    return(
        <ScrollView style={styles.scrollContainer}>
            <View style={{marginTop: 10, alignItems: 'center'}}>
                <Card 
                    borderRadius={15}
                    width= '90%'
                    //containerStyle={{ backgroundColor: 'white' }}
                    title='RESERVAR A NOMBRE'
                    image={require('../../assets/rent.jpg')}>
                <Input 
                    placeholder=' Ingrese su nombre'
                    onChangeText={value => changeName(value)}
                    leftIcon={
                        <Icon
                        name='user-alt'
                        size={20}
                        color='gray'
                        />
                    }
                    errorStyle={{ color: 'red' }}
                    errorMessage={errorName}
                />
                <Input 
                    placeholder='Ingrese su email'
                    onChangeText={value => changeEmail(value)}
                    leftIcon={
                        <Icon
                        name='address-card'
                        size={20}
                        color='gray'
                        />
                    }
                    errorStyle={{ color: 'red' }}
                    errorMessage={errorEmail}
                    keyboardType='email-address'     
                />
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{width: '50%', height: 50,}}>
                        <TouchableOpacity onPress={showDatePicker}>
                            <Input 
                            placeholder={initialDate}
                            editable={false}
                            //keyboardType='decimal-pad'
                            leftIcon={
                                <Icon
                                name='calendar-alt'
                                size={20}
                                color='gray'
                                />
                            }
                            errorStyle={{ color: 'red' }}
                            errorMessage={errorInitialDate}
                            />
                        </TouchableOpacity>
                        <DateTimePickerModal
                            isVisible={isDatePickerVisible}
                            mode="date"
                            //locale="es-ES"
                            onConfirm={handleConfirm}
                            onCancel={hideDatePicker}
                        />
                    </View>
                    <View style={{width: '50%', height: 50,  marginBottom:20}} >
                        <TouchableOpacity onPress={showDatePicker2}>
                            <Input 
                            placeholder={finalDate}
                            editable={false}
                            selectTextOnFocus={false}
                            leftIcon={
                                <Icon
                                name='calendar-times'
                                size={20}
                                color='gray'
                                />
                            }
                            errorStyle={{ color: 'red' }}
                            errorMessage={ errorFinalDate}
                            />

                        </TouchableOpacity>
                        <DateTimePickerModal
                            isVisible={isDatePickerVisible2}
                            mode="date"
                            onConfirm={handleConfirm2}
                            onCancel={hideDatePicker2}
                        />
                    </View>
                </View>
                <Input 
                    placeholder=' Ingrese un celular'
                    onChangeText={value => changeCellphone(value)}
                    leftIcon={
                        <Icon
                        name='phone'
                        size={20}
                        color='gray'
                        />
                    }
                    errorStyle={{ color: 'red' }}
                    errorMessage={errorCellphone}
                    keyboardType='decimal-pad'
        
                />
                <Divider style={{ backgroundColor: 'gold' , marginBottom:5}} />
                <Button
                    icon={<Icon name='credit-card' color='#efb810' size={15} />}
                    buttonStyle={{backgroundColor: 'black', borderRadius: 0, marginHorizontal: 1, marginBottom: 0}}
                    onPress={() => validacion()}
                    title=' Agregar' 
                    type='solid'
                    titleStyle={{color: '#efb810'}}
                    />
                </Card>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322'
    },
    container:{
        flex: 1,
        backgroundColor: '#EFF0F0',
        marginHorizontal: 20,
        marginTop: 10,
        borderRadius: 20,
        padding: 15,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
    },
    title: {
        color: '#efb810',
        fontSize: 42,
        fontWeight: 'bold'
    },
    inputStyle: {
        color: '#262322'
    },
    btnLogin: {
        marginHorizontal: 30,
        marginBottom: 10,
        backgroundColor: 'black'
    },
    paddingBottom:{
        marginBottom: 40,
    },
    datePicker:{
        backgroundColor: 'white',
    }

});

export default RentProcess1