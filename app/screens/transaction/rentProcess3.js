import React, { useState, useEffect } from 'react';
import { StyleSheet, View, ScrollView, ActivityIndicator } from 'react-native';
import { Input, Button, Icon, Overlay, Card, Image, Text } from 'react-native-elements';
import firebase from '../../config/firebase';
import 'firebase/firestore';
import moment from 'moment';

function RentProcess3(props) {
    const { route, navigation } = props;
    const { precio, creditCard, type, name, email, cellphone, initialDate, finalDate, apartmentName, apartmentLocation } = route.params;
    const [userName, setUserName] = useState('');
    const [creditCardStored, setCreditCardStored] = useState('');
    const [uid, setUID] = useState('');
    const [transactionUID, setTransactionUID] = useState('');
    const [proccess, setProcess] = useState(false);
    const [error, setError] = useState('');
    const user = firebase.auth().currentUser;
    const dateNow = moment();

    useEffect(() => {
        setError('');
        //getAuthUser();
        //generateUID();
        storeTransaction();
    }, []);

    //Retreiving user auth info for setting base variables to push into Firebase
    const getAuthUser = async () => {
        try {
            const user = await firebase.auth().currentUser;
            const email = user.providerData[0].email;
            const displayName = user.providerData[0].displayName;
            const uid = user.uid;
            setUserName(user.displayName);
            setCreditCardStored(creditCard);
            setUID(uid);
        } catch (error) {
            console.log(error);
        }
    };

    //Sending Transaction to Firebase Storeage, apending message inside Auth user UID
    const storeTransaction = () => {
        try {
            const db = firebase.firestore()
            db.settings({ experimentalForceLongPolling: true })
            db.collection('transactions').add({
                receiverName: name,
                receiverEmail: email,
                receiverCellphone: cellphone,
                receiverOcupationDate: initialDate,
                receiverEndingDate: finalDate,
                generatedBy: user.uid,
                generatedByName: user.displayName,
                transactionType: type,
                creditCard: creditCard,
                apartmentName: apartmentName,
                apartmentLocation: apartmentLocation,
                precio: precio
            }).then((docRef) => {
                let transactionGeneratedID = docRef.id;
                db.collection('users').doc(firebase.auth().currentUser.uid).set({
                    transactionID: firebase.firestore.FieldValue.arrayUnion(transactionGeneratedID),
                }, { merge: true }).then(() => setProcess(true));
                console.log('nuevo registro');
            });

            //[{ who: "third@test.com", when: new Date() }]
            //firebase.firestore.FieldValue.arrayUnion("greater_virginia")
            /*
            db.collection('users').doc(firebase.auth().currentUser.uid).set({
                receiverName:name,
                receiverEmail:email,
                receiverCellphone:cellphone,
                receiverOcupationDate:initialDate,
                receiverEndingDate:finalDate,
                generatedBy: uid,
                generatedByName:userName,
                transactionType:type,
                creditCard:creditCard,
                apartmentName: apartmentName, 
                apartmentLocation: apartmentLocation,
                transactionUID:transactionUID,
            }).then(() => {
                console.log('registro enviado');
                setCreditCardStored('');
                navigation.navigate('RentProcess2');
            });
            */
        } catch (error) {
            console.log("salio un error:", error);
            setError('Ocurrió un error inesperado, por favor vuelva a intentarlo más tarde.')
        }
    }
    //generating IDs
    const generateUID = () => {
        var S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        var generated = (S4 + S4 + "-" + S4 + "-" + S4 + "-" + S4 + "-" + S4 + S4 + S4);
        setTransactionUID(generated);
    }
    //Testing and webing button
    const aea = () => {
        console.log(creditCardStored);
        console.log(dateNow);
        console.log(transactionUID);
    }

    return (
        <ScrollView style={styles.scrollContainer}>
            <Overlay
                isVisible={proccess}
                windowBackgroundColor="rgba(255, 255, 255, .5)"
                overlayBackgroundColor="red"
                width='auto'
                height="auto"
            >
                <View style={{ alignContent: 'center', alignSelf: 'center', justifyContent: 'center' }}>
                    <Text h4 style={{ alignSelf: 'center' }}> Transaccion completa!</Text>
                    <Image style={{ alignContent: 'center' }}
                        source={require('../../assets/check.png')}
                        style={{ width: 270, height: 270 }}
                    />
                    <Text style={{ marginTop: 10, marginBottom: 20, alignSelf: 'center' }}>Su transacion Nro: se realizo con exito!</Text>

                    <Button
                        buttonStyle={{ backgroundColor: '#efb810', marginTop: 10, marginLeft: 0, marginRight: 0, marginBottom: 0, borderRadius: 5 }}
                        icon={<Icon name='check-circle' color='#ffffff' size={15} />}
                        onPress={() => navigation.navigate('Home')}
                        title=' Aceptar'
                        type='solid'
                        titleStyle={{ color: 'white' }}
                    />
                </View>

            </Overlay>
            <View style={{ marginTop: 30, marginHorizontal: 45, alignItems: 'center' }}>
                <Text style={styles.title}>Estamos procesando su pedido,</Text>
                <Text style={{ color: 'white', fontSize: 18 }}>esta acción no toma mucho tiempo espere unos segundos por favor.</Text>
                <ActivityIndicator size='large' color='#efb810' />
                <Text style={{ color: 'white', fontSize: 18 }}>{error}</Text>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322'
    },
    container: {
        flex: 1,
        backgroundColor: '#EFF0F0',
        marginHorizontal: 20,
        marginTop: 10,
        borderRadius: 20,
        padding: 15,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
    },
    title: {
        color: '#efb810',
        fontSize: 42,
        fontWeight: 'bold'
    },
    inputStyle: {
        color: '#262322'
    },
    title: {
        color: '#efb810',
        fontSize: 28,
        fontWeight: 'bold'
    },
    btnLogin: {
        marginHorizontal: 30,
        marginBottom: 10,
        backgroundColor: 'black'
    }
});

export default RentProcess3
