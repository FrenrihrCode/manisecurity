import React from 'react';
import { StyleSheet, ActivityIndicator, View, ScrollView, Text, Dimensions } from 'react-native';
import { Image, Button, Icon } from 'react-native-elements';
import Swiper from 'react-native-swiper'
const { width, height } = Dimensions.get('window')

function SingleDepartment(props) {
    const { route, navigation } = props
    const { department } = route.params

    return (
        <ScrollView style={styles.scrollContainer}>
            <View style={styles.container1}>

                <Swiper
                    style={styles.wrapper}
                    height={210}
                    paginationStyle={{
                        bottom: -23,
                    }}
                    loop={true}
                    dot={
                        <View
                            style={{
                                backgroundColor: 'rgba(255,255,255,.3)',
                                width: 13,
                                height: 13,
                                borderRadius: 7,
                                marginLeft: 7,
                                marginRight: 7
                            }}
                        />
                    }
                    activeDot={
                        <View
                            style={{
                                backgroundColor: '#fff',
                                width: 13,
                                height: 13,
                                borderRadius: 7,
                                marginLeft: 7,
                                marginRight: 7
                            }}
                        />
                    }
                >
                    <View>
                        <Image
                            style={styles.image}
                            PlaceholderContent={<ActivityIndicator/>}
                            source={{ uri: department.imagen }}
                            resizeMode="cover"
                        />
                    </View>
                    <View>
                        <Image
                            style={styles.image}
                            PlaceholderContent={<ActivityIndicator/>}
                            source={{ uri: department.imagen2 }}
                            resizeMode="cover"
                        />
                    </View>
                    <View>
                        <Image style={styles.image}
                            PlaceholderContent={<ActivityIndicator/>}
                            source={{ uri: department.imagen3 }}
                            resizeMode="cover"
                        />
                    </View>
                </Swiper>
            </View>
            <Text></Text>
            <View style={styles.container}>
                <View style={{ padding: 10 }}>
                    <Text style={styles.title}>{department.nombre}</Text>
                    <Text style={{ color: '#efb810', fontSize: 18 }}>{department.descripcion}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon name='place' color='white' />
                        <Text style={{ color: 'white', fontSize: 16, marginLeft: 8 }}>{department.direccion}</Text>
                    </View>
                    <Button type='solid' title='Ver mapa'
                                buttonStyle={{backgroundColor: 'black', marginTop: 5}} titleStyle={{color: '#efb810'}}
                                onPress={()=> navigation.navigate('LocationDepartment', {department: department})} />
                </View>
                <Text style={{ color: 'white', fontSize: 16, marginLeft: 8, fontWeight: 'bold' }}>Características del departamento</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <View style={styles.descContainer}>
                        <Icon type='material-community' name='floor-plan' />
                        <Text style={styles.desc}>{department.pisos} pisos</Text>
                    </View>
                    <View style={styles.descContainer}>
                        <Icon type='font-awesome' name='bath' />
                        <Text style={styles.desc}>{department.bath} baños</Text>
                    </View>
                    <View style={styles.descContainer}>
                        <Icon type='font-awesome' name='bed' />
                        <Text style={styles.desc}>{department.camas} dormitorios</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    {department.garaje ?
                        <View style={styles.descContainer}>
                            <Icon type='material-community' name='garage' />
                            <Text style={styles.desc}>Garaje</Text>
                        </View>
                        : null
                    }
                    {department.internet ?
                        <View style={styles.descContainer}>
                            <Icon type='font-awesome' name='wifi' />
                            <Text style={styles.desc}>Internet</Text>
                        </View>
                        : null
                    }
                    {department.tv ?
                        <View style={styles.descContainer}>
                            <Icon type='font-awesome' name='tv' />
                            <Text style={styles.desc}>TV</Text>
                        </View>
                        : null}
                </View>
                <View style={{ alignItems: 'center', marginBottom: 8 }}>
                    <Text style={styles.title}>Precio: S/.{department.precio}</Text>
                    {department.disponibilidad ?
                        <Button type='solid'
                            onPress={()=>navigation.navigate('RentProcess1', {precio: department.precio, apartmentName: department.nombre, apartmentLocation: department.direccion})}
                            title='RENTAR'
                            raised
                            buttonStyle={{ backgroundColor: 'black' }}
                            titleStyle={{ color: '#efb810', fontWeight: 'bold' }} />
                        : <Text style={{ color: 'white', fontSize: 14 }}>Este departamento ya fue rentado</Text>
                    }
                </View>
            </View>


        </ScrollView>
    )
}
const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#423f3e'
    },
    container: {
        flex: 1
    },
    title: {
        color: '#efb810',
        fontSize: 28,
        fontWeight: 'bold'
    },
    desc: {
        color: '#262322',
        fontSize: 16
    },
    descContainer: {
        flex: 1,
        margin: 5,
        padding: 5,
        shadowColor: '#262322',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#efb810'
    },

    //Swipper
    wrapper: {
        // backgroundColor: '#f00'
    },
    container1: {
        flex: 1,
        alignItems: 'center'
    },
    image: {
        width: '100%',
        height: 220,
    }
});

export default SingleDepartment