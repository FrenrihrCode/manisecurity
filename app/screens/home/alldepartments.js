import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native'
import { Badge, Image, Button } from 'react-native-elements'
import Firebase from '../../config/firebase'
import 'firebase/firestore'

export default function Alldepartments(props) {
    const { route, navigation } = props
    const { buscar } = route.params
    const [deparments, setDepartments] = useState([])
    const [depas, setDepas] = useState([])
    const [filtertype, setFiltertype] = useState('')

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setDepartments([])
            getResponse().then((data) => {
                setDepartments(data)
                setDepas(data)
            });
        });
        return () => { unsubscribe() }
    }, [])

    const getResponse = async () => {
        try {
            console.log("effect")
            let col = []
            const db = Firebase.firestore()
            db.settings({ experimentalForceLongPolling: true })
            if (buscar == '') {
                const data = await db.collection('departments').limit(10).get()
                data.docs.forEach(doc => {
                    col.push({ _id: doc.id, ...doc.data() })
                })
                return col
            }
            else {
                const data = await db.collection('departments').where(buscar, "==", true).get()
                data.docs.forEach(doc => {
                    col.push({ _id: doc.id, ...doc.data() })
                })
                return col
            }
        } catch (error) {
            console.log(error)
        }
    }

    const orderby = () => {
        setFiltertype('precio')
        let sorteddata = depas.sort((a, b) => parseFloat(a.precio) - parseFloat(b.precio))
        setDepartments(sorteddata)
    }
    const orderby2 = () => {
        setFiltertype('unprecio')
        let sorteddata = depas.sort((a, b) => parseFloat(b.precio) - parseFloat(a.precio))
        setDepartments(sorteddata)
    }

    return (
        <View style={styles.scrollContainer}>
            <Text style={{ marginHorizontal: 20, marginTop: 10, fontSize: 18, fontWeight: 'bold' }}>Ordernar por:</Text>
            <View style={{
                flexDirection: 'row', justifyContent:'space-around',
                marginHorizontal: 20, marginVertical: 5, alignItems: 'center'
            }}>
                <Button
                    onPress={() => orderby()}
                    title="Precio menor"
                    type="outline"
                />
                <Button
                    onPress={() => orderby2()}
                    title="Precio mayor"
                    type="outline"
                />
            </View>
            <FlatList
                data={deparments}
                extraData={filtertype}
                showsHorizontalScrollIndicator={false}
                legacyImplementation={false}
                numColumns={1}
                keyExtractor={(item) => {
                    return item.nombre;
                }}
                renderItem={({ item }) => {
                    return (
                        <View style={styles.center}>
                            <Image
                                source={{ uri: item.imagen }}
                                style={{ width: '100%', height: 200 }}
                                containerStyle={{ borderRadius: 20 }}
                                PlaceholderContent={<ActivityIndicator />}
                            />
                            <Badge value={item.disponibilidad ? 'Por rentar' : 'Rentado'} status={item.disponibilidad ? 'success' : 'error'}
                                containerStyle={{ position: 'absolute', end: 5, top: 5 }} />
                            <View style={styles.card}>
                                <View style={{ flex: 1, padding: 12 }}>
                                    <Text style={{ color: '#efb810', fontSize: 18, fontWeight: 'bold' }} >S/. {item.precio}</Text>
                                    <Text style={{ color: 'white', fontSize: 16, fontWeight: 'bold' }}>{item.nombre}</Text>
                                    <Text style={{ color: 'white', fontSize: 12 }}>{item.direccion}</Text>
                                </View>
                                <View style={{
                                    flex: 0.3, backgroundColor: '#efb810', borderTopEndRadius: 15,
                                    borderBottomEndRadius: 15, justifyContent: 'center'
                                }}>
                                    <TouchableOpacity style={{ padding: 5 }}
                                        onPress={() => navigation.navigate('SingleDepartment', { department: item })}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 12, textAlign: 'center' }}>Más detalles</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    )
                }}
                ListEmptyComponent={() => (<ActivityIndicator style={{ marginTop: 25 }} size='large' animating={true} color='#efb810' />)}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: 'white',
        flex: 1
    },
    card: {
        backgroundColor: 'black',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        end: 0,
        shadowColor: '#262322',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
        width: 275,
        borderRadius: 15,
        backgroundColor: "#262322",
        flexBasis: '42%'
    },
    center: {
        marginHorizontal: 20,
        justifyContent: 'center',
        marginVertical: 10
    }
})
