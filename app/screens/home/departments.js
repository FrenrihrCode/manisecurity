import React, { useEffect, useState } from 'react';
import { StyleSheet, View, FlatList, TouchableOpacity, Text, ActivityIndicator, RefreshControl } from 'react-native';
import { Image, Button, Badge, Icon } from 'react-native-elements';
import Firebase from '../../config/firebase';
import 'firebase/firestore';

function Deparment(props) {
    const { navigation } = props;
    const [deparments, setDepartments] = useState([]);
    const [refreshing, setRefreshing] = React.useState(false);

    useEffect(() => {
        getResponse().then((data) => setDepartments(data))
        /*
        const unsubscribe = navigation.addListener('focus', () => {
            setDepartments([])
            getResponse();
        });
        return () => { unsubscribe() }*/
    }, []);

    const getResponse = async () => {
        try {
            console.log("effect")
            let col = []
            const db = Firebase.firestore()
            db.settings({ experimentalForceLongPolling: true })
            const data = await db.collection('departments').orderBy("puntuacion").limit(5).get()
            data.docs.forEach(doc => {
                col.push({ _id: doc.id, ...doc.data() })
            })
            return col
        } catch (error) {
            console.log(error)
        }
    };

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setDepartments([]);
        getResponse().then((data) => {
            setDepartments(data);
            setRefreshing(false);
        })
    }, [refreshing]);

    const buscar = (que) => {
        navigation.navigate('SearchDep', { buscar: que })
    }

    return (
        <View style={styles.scrollContainer}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', 
                marginHorizontal: 20, marginVertical: 10, alignItems: 'center' }}>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Opciones de búsqueda</Text>
            </View>
            <View style={{ flexDirection: 'row', marginHorizontal: 10, justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity style={styles.iconbtn} onPress={()=>buscar('tv')}>
                    <Icon
                        name='tv'
                        type='font-awesome' />
                    <Text style={{ fontSize: 9, fontWeight: 'bold' }} >TV</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconbtn} onPress={()=>buscar('garaje')}>
                    <Icon
                        name='car'
                        type='font-awesome' />
                    <Text style={{ fontSize: 9, fontWeight: 'bold'}} >GARAJE</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconbtn} onPress={()=>buscar('disponibilidad')}>
                    <Icon
                        name='flag-checkered'
                        type='font-awesome' />
                    <Text style={{ fontSize: 9, fontWeight: 'bold' }} >DISPONIBLE</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconbtn} onPress={()=>buscar('internet')}>
                    <Icon
                        name='wifi'
                        type='font-awesome' />
                    <Text style={{ fontSize: 9, fontWeight: 'bold' }} >INTERNET</Text>
                </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', margin: 5, justifyContent: 'space-between', paddingHorizontal: 15, alignItems: 'center' }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Los mejores</Text>
                <Button type='clear' title='VER MÁS' titleStyle={{ color: '#efb810' }} onPress={()=>buscar('')}></Button>
            </View>
            <FlatList
                data={deparments}
                showsHorizontalScrollIndicator={false}
                legacyImplementation={false}
                numColumns={1}
                refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
                keyExtractor={(item) => {
                    return item.nombre;
                }}
                renderItem={({ item }) => {
                    return (
                        <View style={styles.center}>
                            <Image
                                source={{ uri: item.imagen }}
                                style={{ width: '100%', height: 200 }}
                                containerStyle={{ borderRadius: 20 }}
                                PlaceholderContent={<ActivityIndicator />}
                            />
                            <Badge value={item.disponibilidad ? 'Por rentar' : 'Rentado'} status={item.disponibilidad ? 'success' : 'error'}
                                containerStyle={{ position: 'absolute', end: 5, top: 5 }} />
                            <View style={styles.card}>
                                <View style={{ flex: 1, padding: 12 }}>
                                    <Text style={{ color: '#efb810', fontSize: 18, fontWeight: 'bold' }} >S/. {item.precio}</Text>
                                    <Text style={{ color: 'white', fontSize: 16, fontWeight: 'bold' }}>{item.nombre}</Text>
                                    <Text style={{ color: 'white', fontSize: 12 }}>{item.direccion}</Text>
                                </View>
                                <View style={{
                                    flex: 0.3, backgroundColor: '#efb810', borderTopEndRadius: 15,
                                    borderBottomEndRadius: 15, justifyContent: 'center'
                                }}>
                                    <TouchableOpacity style={{ padding: 5 }}
                                        onPress={() => navigation.navigate('SingleDepartment', { department: item })}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 12, textAlign: 'center' }}>Más detalles</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    )
                }}
                ListEmptyComponent={() => (<ActivityIndicator style={{ marginTop: 25 }} size='large' animating={true} color='#efb810' />)}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: 'white',
        flex: 1
    },
    card: {
        backgroundColor: 'black',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        end: 0,
        shadowColor: '#262322',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
        width: 275,
        borderRadius: 15,
        backgroundColor: "#262322",
        flexBasis: '42%'
    },
    center: {
        marginHorizontal: 20,
        justifyContent: 'center',
        marginBottom: 20
    },
    iconbtn:{
        flex: 1,
        marginHorizontal: 5,
        padding: 5,
        backgroundColor: '#efb810',
        alignContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        elevation: 10
    }
})

export default Deparment;