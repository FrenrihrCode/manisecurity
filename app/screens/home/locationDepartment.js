import React, {Component} from 'react';
import {PermissionsAndroid} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps'
import {Text, View,Image,Alert, Button,Switch, StyleSheet,TouchableOpacity, ScrollView,TextInput, TouchableWithoutFeedback,Dimensions} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import MapViewDirections from 'react-native-maps-directions';
import Geolocation from '@react-native-community/geolocation';

//Geolocation.getCurrentPosition(info => console.log(info));
const width =  Dimensions.get('window').width
const height = Dimensions.get('window').height
const heightbotones = height-200
export default class LocationDepartment extends Component {
    constructor(props) {
      super(props)
      this.state = {
        latitudeUser: 0,
        longitudUser: 0,
        Titulo:"",
        imagen:"",
        focusedLocation: {
          latitude: 36.14319077106534,
          longitude: -86.76708101838142,
          latitudeDelta: 0.00522,
          longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * 0.00522
        }
      };
    }
    onPressZoomIn() {
      this.region = {
        latitude: this.state.focusedLocation.latitude,
        longitude: this.state.focusedLocation.longitude,
        latitudeDelta: this.state.focusedLocation.latitudeDelta * 2,
        longitudeDelta: this.state.focusedLocation.longitudeDelta * 2
      }
  
      this.setState({
        focusedLocation: {
          latitudeDelta: this.region.latitudeDelta,
          longitudeDelta: this.region.longitudeDelta,
          latitude: this.region.latitude,
          longitude: this.region.longitude
        }
      })
      this.map.animateToRegion(this.region, 100);
    }
  
    onPressZoomOut() {
      this.region = {
        latitude: this.state.focusedLocation.latitude,
        longitude: this.state.focusedLocation.longitude,
        latitudeDelta: this.state.focusedLocation.latitudeDelta / 2,
        longitudeDelta: this.state.focusedLocation.longitudeDelta / 2
      }
      this.setState({
        focusedLocation: {
          latitudeDelta: this.region.latitudeDelta,
          longitudeDelta: this.region.longitudeDelta,
          latitude: this.region.latitude,
          longitude: this.region.longitude
        }
      })
      this.map.animateToRegion(this.region, 100);
    }
  
    onMapLayout (){
      const { department } = this.props.route.params;
      /*const { longitud } = this.props.route.params;
      const { latitud } = this.props.route.params;
*/    const Lot = parseFloat(department.lng);
      const Lat = parseFloat(department.lat);
      this.setState({Titulo:department.nombre,imagen:department.imagen });
      this.setState({
        focusedLocation: {
          latitude: Lat,
          longitude: Lot,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }
      })
    }

    /*async permission () {
        try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
              {
                title: 'Device current location permission',
                message:
                  'Allow app to get your current location',
                buttonNeutral: 'Ask Me Later',
                buttonNegative: 'Cancel',
                buttonPositive: 'OK',
              },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              this.getCurrentLocation();
            } else {
              console.log('Location permission denied');
            }
          } catch (err) {
            console.warn(err);
          }
    }*/
    /*getCurrentLocation(){
        Geolocation.requestAuthorization();
        Geolocation.getCurrentPosition(
           (position) => {
            this.setState({
                latitudeUser:position.coords.latitude,longitudUser:position.coords.longitude
              })
           },
           (error) => {
             console.log("map error: ",error);
               console.log(error.code, error.message);
           },
           { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000 }
       );
      }*/

    async componentDidMount(){
        await this.onMapLayout()
        var that =this;
        //Checking for the permission just after component loaded
        if(Platform.OS === 'ios'){
          this.callLocation(that);
        }else{
          async function requestLocationPermission() {
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{
                  'title': 'Location Access Required',
                  'message': 'This App needs to Access your location'
                }
              )
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                //To Check, If Permission is granted
                that.callLocation(that);
              } else {
                alert("Permission Denied");
              }
            } catch (err) {
              alert("Error",err);
              console.warn(err)
            }
          }
          requestLocationPermission();
        }
    }
    callLocation(that){
        //alert("callLocation Called");
          Geolocation.getCurrentPosition(
            //Will give you the current location
             (position) => {
                const currentLongitude = parseFloat(JSON.stringify(position.coords.longitude));
                //getting the Longitude from the location json
                const currentLatitude = parseFloat(JSON.stringify(position.coords.latitude));
                //getting the Latitude from the location json
                that.setState({ longitudUser:currentLongitude });
                //Setting state Longitude to re re-render the Longitude Text
                that.setState({ latitudeUser:currentLatitude });
                //Setting state Latitude to re re-render the Longitude Text
             },
             (error) => {
               //alert(error.message)
                console.warn(error)
              },{enableHighAccuracy: true, timeout: 2000 }
          );
          that.watchID = Geolocation.watchPosition((position) => {
            //Will give you the location on location change
              console.log(position);
              const currentLongitude = parseFloat(JSON.stringify(position.coords.longitude));
              //getting the Longitude from the location json
              const currentLatitude = parseFloat(JSON.stringify(position.coords.latitude));
              //getting the Latitude from the location json
             that.setState({ longitudUser:currentLongitude });
             //Setting state Longitude to re re-render the Longitude Text
             that.setState({ latitudeUser:currentLatitude });
             //Setting state Latitude to re re-render the Longitude Text
          });
       }
       componentWillUnmount = () => {
          Geolocation.clearWatch(this.watchID);
       }
      
        
         /* 

         }*/
        
    
  
    render() {
      const GOOGLE_MAPS_APIKEY = 'AIzaSyCr_aCfPUQCPLC9TPvXmqDN08tv_ZnrsrI';
      const origin = {latitude: this.state.latitudeUser, longitude: this.state.longitudUser};
      const destination = {latitude: this.state.focusedLocation.latitude, longitude: this.state.focusedLocation.longitude};
      return (
        <ScrollView>
          <View style={styles.container}>
            <MapView
              region={this.state.focusedLocation}
              provider={PROVIDER_GOOGLE}
              style={styles.map}
              onPress={this.pickLocationHandler}
              showsUserLocation={true}
              followUserLocation={true}
              zoomEnabled={true}
              mapType='standard'
              ref={ref => this.map = ref}
            >
               
                <Marker
                    coordinate={{
                        latitude: this.state.focusedLocation.latitude,
                        longitude: this.state.focusedLocation.longitude,
                    }}>
                    <Image source = {{uri: this.state.imagen}} style={{height: 50, width:50, borderBottomRightRadius:25, borderBottomLeftRadius:25, borderWidth: 5,borderColor:'#efb810'}} />
                    <Callout>
                        <View
                            style={{
                                backgroundColor: "#FFFFFF",
                                borderRadius: 5,
                            }}>
                            <Text>
                                {this.state.Titulo}
                            </Text>
                        </View>
                    </Callout>
                </Marker>
                <Marker
                    pinColor="#efb810"
                    coordinate={{
                        latitude: this.state.latitudeUser,
                        longitude:this.state.longitudUser,
                        
                    }}>
                    <Callout>
                        <View
                            style={{
                                backgroundColor: "#FFFFFF",
                                borderRadius: 5,
                            }}>
                            <Text>
                                Ubicación Actual
                            </Text>
                        </View>
                    </Callout>
                </Marker>
                <MapViewDirections
                  origin={origin}
                  destination={destination}
                  apikey={GOOGLE_MAPS_APIKEY}
                  strokeWidth={3}
                  strokeColor="#800000"
                />
                </MapView>
                 
                <View style={styles.button} >
                  <TouchableOpacity
                      style={styles.zoomIn}
                      onPress={() => { this.onPressZoomOut() }}
                    >
                      <MaterialCommunityIcons
                        name="zoom-in"
                        style={styles.icon}
                        size={20}
                        color="#fff"
                      />
                  </TouchableOpacity>
                  <TouchableOpacity
                        style={styles.zoomOut}
                        onPress={() => { this.onPressZoomIn() }}
                      >
                        <MaterialCommunityIcons
                          name="zoom-out"
                          style={styles.icon}
                          size={20}
                          color="#fff"
                        />
                  </TouchableOpacity>
              </View>
            </View>
        </ScrollView>
          )
      }
  }
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F2C53D',
    },
    map: {
      height: height,
      width: width,
    },
    search:{
      position: "absolute",
    },
    button: {
      top: heightbotones,
      backgroundColor: '#efb810',
      position: "absolute",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-around",
      padding: 5,
      width:150,
      borderRadius: 10
    },
});