import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, ScrollView, Alert, } from 'react-native';
import { Input, Button, Rating } from 'react-native-elements';

export default function Recomendaciones({ navigation }) {
    const [isReference, setReference] = useState('');

    let datos = {
        reference: isReference,
    };

    function Next() {
        if (isReference == '') {
            Alert.alert('Alerta', 'El campo es obligatorio');
        } else {
            
            navigation.navigate('Response', {
                datos,
            });
        }
    }
    return (
        <ScrollView style={styles.scrollContainer}>
            <View style={{ alignItems: 'center', marginTop: 20 }}>
                <Text style={styles.title}>Ayudanos a mejorar,</Text>
                <Text style={{ color: 'white', fontSize: 18 }}>envíanos un mensaje</Text>
            </View>
            <View style={styles.container}>
                <Rating
                    type='rocket'
                    ratingCount={5}
                    imageSize={40}
                    showRating
                />
                <Input
                    placeholderTextColor='#A7AEB5'
                    placeholder='Tu mensaje va aquí'
                    onChangeText={(text) => setReference(text)}
                    value={isReference}
                    inputStyle={styles.inputStyle}
                    leftIcon={{ type: 'material', name: 'mail', color: '#506982' }}
                />
                 <Button buttonStyle={styles.btnLogin} titleStyle={{color: '#efb810'}} title='ENVIAR' onPress={Next}/>
                </View>

            </ScrollView>

            );
          }
          
const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322'
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        marginHorizontal: 16,
        marginTop: 20,
        borderRadius: 20,
        paddingHorizontal: 10,
        paddingVertical: 15,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
        alignItems: 'center'

    },
    title: {
        color: '#efb810',
        fontSize: 28,
        fontWeight: 'bold'
    },
    inputStyle: {
        color: '#262322',
        fontSize: 16,
        alignItems: 'center',
    },
    btnLogin: {
        marginHorizontal: 10,
        backgroundColor: '#262322'
    }
});
