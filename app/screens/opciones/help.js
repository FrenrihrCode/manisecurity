import React, { Component } from 'react';
import {
    Alert,
    StyleSheet,
    ScrollView,
    ActivityIndicator,
    View,
    TouchableOpacity
} from 'react-native'
import { Input, Button, Icon, Text, Image } from 'react-native-elements';
import { Avatar } from 'react-native-elements';
import Swiper from 'react-native-swiper'
class help extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <ScrollView style={styles.scrollContainer}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.title}>Tienes dudas,</Text>
                    <Text style={{ color: 'white', fontSize: 18 }}>aquí posees un pequeño tutorial</Text>

                    <View style={styles.container}>
                        <View style={styles.container1}>

                            <Swiper
                                style={styles.wrapper}
                                height={475}
                                width={310}
                                alignContent={'center'}
                                paginationStyle={{
                                    bottom: -50,
                                }}
                                loop={true}

                                dot={
                                    <View
                                        style={{
                                            backgroundColor: '#FFF176',
                                            width: 13,
                                            height: 13,
                                            borderRadius: 7,
                                            marginLeft: 7,
                                            marginRight: 7
                                        }}
                                    />
                                }
                                activeDot={
                                    <View
                                        style={{
                                            backgroundColor: '#efb810',
                                            width: 13,
                                            height: 13,
                                            borderRadius: 7,
                                            marginLeft: 7,
                                            marginRight: 7
                                        }}
                                    />
                                }
                            >
                                <View
                                    title={<Text style={{alignContent:'center'}}>Busca el departamento de tus sueños en la sección Inicio.</Text>}
                                >
                                    <Image style={styles.image}
                                        source={require('../../assets/paso1.png') } />
                                </View>
                                <View
                                    title={<Text>Revisa los datos del dapartamento, si este encaja a tus deseos rentalo.</Text>}
                                >
                                    <Image style={styles.image}
                                        source={require('../../assets/paso2.png')} />
                                </View>
                                <View
                                    title={<Text>Llena el formulario para proceder a rentar el departamento.</Text>}
                                >
                                    <Image style={styles.image}
                                        source={require('../../assets/paso3.png')} />
                                </View>
                                <View
                                    title={<Text>Revisa la transacción realizada en la sección Transacciones.</Text>}
                                >
                                    <Image style={styles.image}
                                        source={require('../../assets/paso4.png')} />
                                </View>
                            </Swiper>
                            <Text></Text>
                            <Text></Text>
                            <Text></Text>
                        <Button buttonStyle={styles.btnLogin} titleStyle={{ color: '#efb810' }} title='Regresar' onPress={() => this.props.navigation.goBack()} />
                        </View>
                    </View>
                        
                    </View>

            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322'
    },
    title: {
        color: '#efb810',
        fontSize: 28,
        fontWeight: 'bold'
    },
    inputStyle: {
        color: '#262322',
        fontSize: 16,
        alignItems: 'center',
    },
    btnLogin: {
        marginHorizontal: 30,
        marginTop: 10,
        backgroundColor: '#262322'
    },
      //Swipper
      wrapper: {
        // backgroundColor: '#f00'
    },
    container1: {
        alignItems: 'center'
    },
    image: {
        width: 310, 
        height: 475,
        borderRadius: 20
    },
    container: {
        backgroundColor: '#EFF0F0',
        marginHorizontal: 16,
        marginTop: 20,
        borderRadius: 20,
        paddingHorizontal: 10,
        paddingVertical: 15,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
        alignItems: 'center'

    },
});

export default help