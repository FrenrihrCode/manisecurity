import React, { Component } from 'react';
import {
    Alert,
    StyleSheet,
    ScrollView,
    ActivityIndicator,
    View,
    TouchableOpacity
} from 'react-native'
import { Input, Button, Icon, Text } from 'react-native-elements';
import { Avatar } from 'react-native-elements';
class about extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {

        return (
            <ScrollView style={styles.scrollContainer}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.title}>Conoce,</Text>
                    <Text style={{ color: 'white', fontSize: 18 }}>a los implicados en el proyecto</Text>
                </View>
                <View style={styles.container}>
                 <Text h4>Integrantes</Text>
                 <Text></Text>
                    <Avatar
                        size="large"
                        rounded
                        source={{
                            uri: 'https://img.freepik.com/free-vector/abstract-dynamic-pattern-wallpaper-vector_53876-59131.jpg?size=338&ext=jpg',
                        }}
                    />
                    <Text>Carlos Quispe Quispe</Text>
                    <Text></Text>
                    <Avatar
                        size="large"
                        rounded
                        source={{
                            uri: 'https://img.freepik.com/free-vector/abstract-dynamic-pattern-wallpaper-vector_53876-59131.jpg?size=338&ext=jpg',
                        }}
                    />
                    <Text>Oswaldo Ñaupa Laura</Text>
                    <Text></Text>
                    <Avatar
                        size="large"
                        rounded
                        source={{
                            uri: 'https://img.freepik.com/free-vector/abstract-dynamic-pattern-wallpaper-vector_53876-59131.jpg?size=338&ext=jpg',
                        }}
                    />
                    <Text>Juan Mancilla Cornejo</Text>
                    <Text></Text>
                    <Avatar
                        size="large"
                        rounded
                        source={{
                            uri: 'https://img.freepik.com/free-vector/abstract-dynamic-pattern-wallpaper-vector_53876-59131.jpg?size=338&ext=jpg',
                        }}
                    />
                    <Text>Alvaro Medina Zumarán</Text>
                    <Text></Text>
                    <Button buttonStyle={styles.btnLogin} titleStyle={{ color: '#efb810' }} title='Regresar' onPress={() => this.props.navigation.goBack()} />
                </View>

            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322'
    },
    container: {
        flex: 1,
        backgroundColor: '#EFF0F0',
        marginHorizontal: 16,
        marginTop: 20,
        borderRadius: 20,
        paddingHorizontal: 10,
        paddingVertical: 15,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
        alignItems: 'center'

    },
    title: {
        color: '#efb810',
        fontSize: 28,
        fontWeight: 'bold'
    },
    inputStyle: {
        color: '#262322',
        fontSize: 16,
        alignItems: 'center',
    },
    btnLogin: {
        marginHorizontal: 30,
        marginTop: 10,
        backgroundColor: '#262322'
    }
});

export default about