import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, Switch, TouchableOpacity, ScrollView, Alert, Image } from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';

export default function Response({ navigation }) {
    return (
        <ScrollView style={styles.scrollContainer}>
            <View style={{ marginTop: 15, alignItems: 'center' }}>
                <Text style={styles.title}>Gracias ayudarnos,</Text>
                <Text style={{ color: 'white', fontSize: 18 }}>hemos reicbido tu mensaje</Text>
            </View>
            <View style={styles.container}>
                <Text></Text>
                <Image source={ require('../../assets/correo.png')} style={{width:200, height:200}}></Image>
                <Button buttonStyle={styles.btnLogin} titleStyle={{ color: '#efb810' }} title='Aceptar'
                    onPress={() => navigation.navigate('Options')}></Button>
                </View>

        </ScrollView>
    );
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322'
    },
    container: {
        flex: 1,
        backgroundColor: '#EFF0F0',
        marginHorizontal: 16,
        marginTop: 20,
        borderRadius: 20,
        paddingHorizontal: 10,
        paddingVertical: 15,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
        alignItems: 'center'

    },
    title: {
        color: '#efb810',
        fontSize: 28,
        fontWeight: 'bold'
    },
    inputStyle: {
        color: '#262322',
        fontSize: 16,
        alignItems: 'center',
    },
    btnLogin: {
        marginHorizontal: 30,
        marginTop: 10,
        backgroundColor: '#262322'
    }
});
