import React, { useState } from 'react';
import { StyleSheet, View, ScrollView, Text, SafeAreaView, TouchableOpacity, Image } from 'react-native';
import { ListItem, Divider, Overlay } from 'react-native-elements';
import Firebase from '../../config/firebase';

function Options(props) {
    const { navigation } = props;
    const [visible, setVisible] = useState(false);
    const user = Firebase.auth().currentUser;

    const Next = () => {
        setVisible(false)
        navigation.navigate('Perfil', {
            user
        });
    }

    return (
        <ScrollView style={styles.scrollContainer}>
            <Overlay isVisible={visible} onBackdropPress={() => setVisible(false)}>
                <View style={{ width: 285 }}>
                    <Text>Datos de mi cuenta</Text>
                    <ListItem
                        leftAvatar={{ source: { uri: user.photoURL }, title: user.displayName[0] }}
                        title={user.displayName}
                        subtitle={user.email}
                    />
                    <TouchableOpacity style={styles.button} onPress={()=>Next()}>
                        <Text style={styles.textButton}>Actualizar perfil</Text>
                    </TouchableOpacity>
                </View>
            </Overlay >
            <View style={{ marginVertical: 8, marginHorizontal: 15 }}>
                <ListItem
                    title='Opciones de perfil'
                    disabled
                    bottomDivider
                />
                <ListItem
                    title='Mi perfil'
                    leftIcon={{ name: 'person' }}
                    onPress={() => setVisible(true)}
                    bottomDivider
                    chevron
                />
                <ListItem
                    title='Cerrar sesión'
                    leftIcon={{ name: 'exit-to-app' }}
                    onPress={()=>Firebase.auth().signOut()}
                    bottomDivider
                    chevron
                />
                <Divider style={{ backgroundColor: 'transparent', height: 8 }} />
                <ListItem
                    title='Opciones de la aplicación'
                    disabled
                    bottomDivider
                />
                <ListItem
                    title='Enviar recomendaciones'
                    leftIcon={{ name: 'message' }}
                    bottomDivider
                    chevron
                    onPress={() => navigation.navigate("Recomendaciones")}
                />
                <ListItem
                    title='Cómo usar RentY'
                    leftIcon={{ name: 'help' }}
                    bottomDivider
                    onPress={() => navigation.navigate("Help")}
                    chevron
                />
                <ListItem
                    title='Acerca de RentY'
                    leftIcon={{ name: 'info' }}
                    onPress={() => navigation.navigate("About")}
                    bottomDivider
                    chevron
                />
            </View>

        </ScrollView>
    );
};

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322',
        flex: 1
    },
    sectionList: {
        backgroundColor: '#EFF0F0',
        marginHorizontal: 20
    },
    button: {
        paddingLeft: 30,
        paddingRight: 30,
        padding: 5,
        marginTop: 5,
        alignSelf: 'center',
        backgroundColor: '#000000',
        borderRadius: 5,
    },
    textButton: {
        color: '#efb810',
        fontSize: 20,
    },
})

export default Options;