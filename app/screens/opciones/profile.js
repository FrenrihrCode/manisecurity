import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text
} from 'react-native';
import Firebase from '../../config/firebase';
import { Input, Button, Avatar } from 'react-native-elements';
import Loader from '../components/loader';

class UserDetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.route.params.user.displayName,
            email: this.props.route.params.user.email,
            mobile: '',
            avatar: this.props.route.params.user.photoURL,
            key: this.props.route.params.user.uid,
            dni: '',
            isLoading: false
        };
    }

    async componentDidMount() {
        this.setState({isLoading: true})
        //const user = Firebase.auth().currentUser;
        const db = Firebase.firestore();
        db.settings({ experimentalForceLongPolling: true });
        await db.collection('users')
            .doc(this.state.key)
            .get()
            .then((res) => {
                if (res.exists) {
                    const user = res.data();
                    this.setState({
                        mobile: user.mobile,
                        dni: user.dni,
                        isLoading: false
                    });
                } else {
                    this.setState({
                        isLoading: false
                    });
                    console.log('Document does not exist!');
                }
            });
     }

    inputValueUpdate = (val, prop) => {
        const state = this.state;
        state[prop] = val;
        this.setState(state);
    };

    updateUser = () => {
        this.setState({
            isLoading: true,
        });
        const updateDBRef = Firebase.firestore().collection('users').doc(this.state.key);
        updateDBRef
            .set({
                name: this.state.name,
                email: this.state.email,
                mobile: this.state.mobile,
                dni: this.state.dni,
            }, { merge: true })
            .then(() => {
                this.setState({
                    isLoading: false
                });
                this.props.navigation.goBack();
            })
            .catch((error) => {
                console.error('Error: ', error);
                this.setState({
                    isLoading: false
                });
            });

        let user = Firebase.auth().currentUser;
        user.updateProfile({
            displayName: this.state.name,
            email: this.state.email,
        });
    }

    render() {
        return (
            <ScrollView style={styles.scrollContainer}>
                <View style={{ marginTop: 10, alignItems: 'center' }}>
                    <Text style={styles.title}>Hecha un vistazo,</Text>
                    <Text style={{ color: 'white', fontSize: 18 }}>personaliza tu perfil</Text>
                </View>
                <Loader loading={this.state.isLoading} />
                <View style={styles.container}>
                    <Avatar
                        size="large"
                        rounded
                        source={{
                            uri: this.state.avatar,
                        }}
                    />
                    <Input
                        placeholderTextColor='#A7AEB5'
                        value={this.state.name}
                        onChangeText={(val) => this.inputValueUpdate(val, 'name')}
                        style={{ flex: 1 }}
                        inputStyle={styles.inputStyle}
                        leftIcon={{ type: 'material', name: 'person', color: '#506982' }}
                        editable={true}
                        label='Nombre de usuario'
                    />
                    <Input
                        placeholderTextColor='#A7AEB5'
                        value={this.state.email}
                        onChangeText={(val) => this.inputValueUpdate(val, 'email')}
                        keyboardType='email-address'
                        inputStyle={styles.inputStyle}
                        leftIcon={{ type: 'material', name: 'email', color: '#506982' }}
                        editable={true}
                        label='Correo electrónico'
                    />
                    <Input
                        placeholderTextColor='#A7AEB5'
                        value={this.state.mobile}
                        onChangeText={(val) => this.inputValueUpdate(val, 'mobile')}
                        keyboardType='numeric'
                        inputStyle={styles.inputStyle}
                        leftIcon={{ type: 'material', name: 'phone', color: '#506982' }}
                        editable={true}
                        label='Número celular'
                    />
                    <Input
                        placeholderTextColor='#A7AEB5'
                        value={this.state.dni}
                        onChangeText={(val) => this.inputValueUpdate(val, 'dni')}
                        keyboardType='numeric'
                        inputStyle={styles.inputStyle}
                        leftIcon={{ type: 'font-awesome', name: 'id-card', color: '#506982' }}
                        editable={true}
                        label='Número DNI'
                    />
                    <Button buttonStyle={styles.btnLogin} titleStyle={{ color: '#efb810' }} title='ACTUALIZAR' onPress={() => this.updateUser()} />
                </View>
            </ScrollView>
        );
    }
}


const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#262322'
    },
    container: {
        flex: 1,
        backgroundColor: '#EFF0F0',
        marginHorizontal: 16,
        marginTop: 20,
        borderRadius: 20,
        paddingHorizontal: 15,
        paddingVertical: 15,
        shadowColor: '#313336',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
        marginVertical: 10,
        flexBasis: '42%',
        alignItems: 'center'

    },
    title: {
        color: '#efb810',
        fontSize: 28,
        fontWeight: 'bold'
    },
    inputStyle: {
        color: '#262322',
        fontSize: 16,
        alignItems: 'center',
    },
    btnLogin: {
        marginHorizontal: 30,
        marginTop: 10,
        backgroundColor: '#262322'
    }
});

export default UserDetailScreen;