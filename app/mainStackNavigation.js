import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
//firebase
import Firebase from './config/firebase';
//Screens
import LoadingScreen from './screens/components/loadingscreen';
//auth pantallas
import Start from './screens/login/start';
import Login from './screens/login/login';
import Resgister from './screens/login/register';
//pestañas propiedades
import Departments from './screens/home/departments';
import SingleDepartment from './screens/home/singleDepartment';
import Alldepartments from './screens/home/alldepartments';
import LocationDepartment from './screens/home/locationDepartment';
//transacciones
import RentProcess1 from './screens/transaction/rentProcess1';
import RentProcess2 from './screens/transaction/rentProcess2';
import RentProcess3 from './screens/transaction/rentProcess3';
//pestañas para ver tus transacciones yep solo eso :v
import Feature from './screens/feature/feature';
import Transaction from './screens/feature/transaction';
import Updatephoto from './screens/feature/updatephoto';
//pestañas Cerca de mi
//pestaña opciones y perfil
import Options from './screens/opciones/options';
import UserDetailScreen from './screens/opciones/profile';
import Recomendaciones from './screens/opciones/recomendaciones';
import Response from './screens/opciones/request';
import About from './screens/opciones/about';
import Help from './screens/opciones/help';

const StackScreenMani = createStackNavigator();
function reportsStack(){
    return (
        <StackScreenMani.Navigator initialRouteName='Home'>
            <StackScreenMani.Screen 
                name='Home'
                component={Departments}
                options={() => (
                    { 
                    title: 'Descubre',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='SearchDep'
                component={Alldepartments}
                options={() => (
                    { 
                        title: 'Busca',
                        headerStyle: {
                            backgroundColor: '#262322',
                        },
                            headerTintColor: '#EFF0F0',
                            headerTitleStyle: {
                            fontWeight: 'bold'
                        },
                    }
                )}
            />
            <StackScreenMani.Screen 
                name='SingleDepartment'
                component={SingleDepartment}
                options={({route}) => (
                    { 
                    title: route.params.department.nombre,
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='LocationDepartment'
                component={LocationDepartment}
                options={({route}) => (
                    { 
                    title: "Mapa " + route.params.department.nombre,
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='RentProcess1'
                component={RentProcess1}
                options={() => (
                    { 
                    title: 'Renta',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='RentProcess2'
                component={RentProcess2}
                options={() => (
                    { 
                    title: 'Renta',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='RentProcess3'
                component={RentProcess3}
                options={() => (
                    { 
                    title: 'Listo',
                    headerLeft: null,
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
        </StackScreenMani.Navigator>
    )
};

function featureStack(){
    return (
        <StackScreenMani.Navigator initialRouteName='Feature'>
            <StackScreenMani.Screen 
                name='Feature'
                component={Feature}
                options={() => (
                    { 
                    title: 'Mis transacciones',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='Transaction'
                component={Transaction}
                options={() => (
                    { 
                    title: 'Ver transacción',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='Updatephoto'
                component={Updatephoto}
                options={() => (
                    { 
                    title: 'Actualizar mi avatar',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
        </StackScreenMani.Navigator>
    )
};

function optionsStack(){
    return (
        <StackScreenMani.Navigator>
            <StackScreenMani.Screen 
                name='Options'
                component={Options}
                options={() => (
                    { 
                    title: 'Opciones de usuario',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                    headerTintColor: '#EFF0F0',
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='Perfil'
                component={UserDetailScreen}
                options={() => (
                    { 
                    title: 'Mi perfil',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='Recomendaciones'
                component={Recomendaciones}
                options={() => (
                    { 
                    title: 'Apoya',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='Response'
                component={Response}
                options={() => (
                    { 
                    title: 'Gracias',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='About'
                component={About}
                options={() => (
                    { 
                    title: 'Acerca de RentY',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
            <StackScreenMani.Screen 
                name='Help'
                component={Help}
                options={() => (
                    { 
                    title: 'Ayuda...',
                    headerStyle: {
                      backgroundColor: '#262322',
                    },
                      headerTintColor: '#EFF0F0',
                      headerTitleStyle: {
                      fontWeight: 'bold'
                    },
                  }
                )}
            />
        </StackScreenMani.Navigator>
    )
};

const Tab = createMaterialBottomTabNavigator();
function HomeTabs() {
  return (
    <Tab.Navigator
        initialRouteName="TabReport"
        activeColor="white"
        barStyle={{ backgroundColor: '#262322' }}  >
        <Tab.Screen name="TabReport" component={reportsStack}
            options={{
                tabBarLabel: 'Inicio',
                tabBarIcon: ({ color }) => (
                    <MaterialIcons name="home" color={color} size={26} />
                ),
            }} />
        <Tab.Screen name="TabFeature" component={featureStack}
            options={{
                tabBarLabel: 'Transacciones',
                tabBarIcon: ({ color }) => (
                    <MaterialIcons name="layers" color={color} size={26} />
                ),
            }} />
        <Tab.Screen name="TabOptions" component={optionsStack}
            options={{
                tabBarLabel: 'Opciones',
                tabBarIcon: ({ color }) => (
                    <MaterialIcons name="settings" color={color} size={26} />
                ),
            }} />
    </Tab.Navigator>
  );
}

const AuthStack = createStackNavigator();
function AuthStackScreen(){
    return (
        <AuthStack.Navigator initialRouteName='Start'>
            <Stack.Screen
                name='Start'
                component={Start}
                options={{headerShown: false}} />
            <Stack.Screen 
                name='Login'
                component={Login}
                options={{headerShown: false}} />
            <Stack.Screen 
                name='Register'
                component={Resgister}
                options={{headerShown: false}} />   
        </AuthStack.Navigator>
    );
}

const Stack = createStackNavigator();
function MainStackNavigator() {
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState(null);

    function onAuthStateChanged(user) {
        setUser(user);
        if (initializing) setInitializing(false);
    }
    
    useEffect(() => {
        const subscriber = Firebase.auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber; // unsubscribe on unmount
    }, []);
      /*
    useEffect(()=>{
        setTimeout(() => {
            setUser({});
        }, 500);
    }, []);*/
    if (initializing) return <LoadingScreen/>;
    return (
        <NavigationContainer>
            <Stack.Navigator >
            {user == null ? (
            // No token found, user isn't signed in
                <Stack.Screen 
                    name='AuthStack'
                    component={AuthStackScreen}
                    options={{headerShown: false}} />
            ) : (
                <Stack.Screen 
                    name='HomeTabs'
                    component={HomeTabs}
                    options={{headerShown: false}} />
            )}
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default MainStackNavigator