import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAAUz6ZgiULq8QsmRzUgYc64pSgSWiqQkk",
    authDomain: "manisecurity-2639a.firebaseapp.com",
    databaseURL: "https://manisecurity-2639a.firebaseio.com",
    projectId: "manisecurity-2639a",
    storageBucket: "manisecurity-2639a.appspot.com",
    messagingSenderId: "143984537778",
    appId: "1:143984537778:web:e08308c365eabe87d919ab",
    measurementId: "G-1R3VKKGZK8"
};

firebase.initializeApp(firebaseConfig);
export default firebase;