import 'react-native-gesture-handler';

import React from 'react';

import MainStackNavigator from './app/mainStackNavigation';

export default function App() {
  return <MainStackNavigator />
}